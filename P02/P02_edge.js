/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'fondo',
            type:'image',
            rect:['0px','0px','969px','629px','auto','auto'],
            opacity:1,
            fill:["rgba(0,0,0,0)",im+"fondo.png",'0px','0px']
         },
         {
            id:'placa_menu',
            type:'image',
            rect:['-12px','149px','109px','323px','auto','auto'],
            opacity:1,
            fill:["rgba(0,0,0,0)",im+"placa_menu.png",'0px','0px']
         },
         {
            id:'boton2',
            type:'image',
            rect:['0px','349px','77px','85px','auto','auto'],
            cursor:['pointer'],
            fill:["rgba(0,0,0,0)",im+"boton2.png",'0px','0px']
         },
         {
            id:'puerta',
            type:'image',
            rect:['868px','35px','67px','64px','auto','auto'],
            cursor:['pointer'],
            fill:["rgba(0,0,0,0)",im+"puerta.png",'0px','0px']
         },
         {
            id:'banner',
            type:'image',
            rect:['320px','595px','330px','111px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"banner.png",'0px','0px']
         },
         {
            id:'boton1',
            type:'image',
            rect:['0px','198px','77px','85px','auto','auto'],
            cursor:['pointer'],
            fill:["rgba(0,0,0,0)",im+"boton1.png",'0px','0px']
         },
         {
            id:'img1',
            type:'image',
            rect:['184px','71px','569px','162px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"img1.png",'0px','0px']
         },
         {
            id:'img22',
            type:'image',
            rect:['262px','274px','497px','141px','auto','auto'],
            fill:["rgba(0,0,0,0)",'img2.png','0px','0px']
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_fondo}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0'],
            ["style", "left", '0px']
         ],
         "${_boton1}": [
            ["style", "top", '198px'],
            ["style", "left", '-97px'],
            ["style", "cursor", 'pointer']
         ],
         "${_img1}": [
            ["style", "top", '-167px'],
            ["style", "opacity", '0'],
            ["style", "left", '184px']
         ],
         "${_img22}": [
            ["style", "top", '274px'],
            ["style", "opacity", '0'],
            ["style", "left", '262px']
         ],
         "${_boton2}": [
            ["style", "top", '349px'],
            ["style", "opacity", '1'],
            ["style", "left", '-97px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '969px'],
            ["style", "height", '629px'],
            ["style", "overflow", 'hidden']
         ],
         "${_puerta}": [
            ["style", "top", '35px'],
            ["style", "opacity", '0'],
            ["style", "left", '868px'],
            ["style", "cursor", 'pointer']
         ],
         "${_banner}": [
            ["style", "left", '320px'],
            ["style", "top", '595px']
         ],
         "${_placa_menu}": [
            ["style", "top", '149px'],
            ["style", "opacity", '1'],
            ["style", "left", '-109px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 4995,
         autoPlay: true,
         timeline: [
            { id: "eid17", tween: [ "style", "${_placa_menu}", "left", '-12px', { fromValue: '-109px'}], position: 1000, duration: 1000 },
            { id: "eid36", tween: [ "style", "${_img22}", "opacity", '1', { fromValue: '0'}], position: 3990, duration: 1005 },
            { id: "eid31", tween: [ "style", "${_img1}", "top", '71px', { fromValue: '-167px'}], position: 3000, duration: 1000 },
            { id: "eid18", tween: [ "style", "${_boton1}", "left", '0px', { fromValue: '-97px'}], position: 1000, duration: 1000 },
            { id: "eid21", tween: [ "style", "${_puerta}", "opacity", '1', { fromValue: '0'}], position: 2000, duration: 1000 },
            { id: "eid16", tween: [ "style", "${_boton2}", "left", '0px', { fromValue: '-97px'}], position: 1000, duration: 1000 },
            { id: "eid24", tween: [ "style", "${_img1}", "opacity", '1', { fromValue: '0'}], position: 3000, duration: 1000 },
            { id: "eid3", tween: [ "style", "${_fondo}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 1000 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-415134790");
